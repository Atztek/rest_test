from django.shortcuts import render
from rest_framework import viewsets
from .serializers import UserSerializer, TestSerializer
from django.contrib.auth.models import User
from .models import Test
# Create your views here.


class UserViewSet(viewsets.ModelViewSet):
    """     A viewset for viewing and editing user instances.     """
    serializer_class = UserSerializer
    queryset = User.objects.all()

class TestViewSet(viewsets.ModelViewSet):
    """     A viewset for viewing and editing user instances.     """
    serializer_class = TestSerializer
    queryset = Test.objects.all()
