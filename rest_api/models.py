from django.db import models

# Create your models here.


class Test(models.Model):
    """Shop model class."""
    name = models.CharField(max_length=250)
    somenumber = models.PositiveSmallIntegerField(default=1)

    def __str__(self):
        return self.name
